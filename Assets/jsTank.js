#pragma strict

var speed = 5;
var rotSpeed = 120;
var turret : GameObject;

var power = 600;
var bullet : Transform;
var explotion : Transform;
var snd : AudioClip;

function Start () {

}

function Update () {
	var amtToMove = speed * Time.deltaTime;
	var amtToRot = rotSpeed * Time.deltaTime;
	
	var front = Input.GetAxis("Vertical");
	var ang = Input.GetAxis("Horizontal");
	var ang2 = Input.GetAxis("MyTank");
	
	transform.Translate(Vector3.forward * front * amtToMove);
	transform.Rotate(Vector3(0, ang*amtToRot, 0));
	turret.transform.Rotate(Vector3.left * ang2 * amtToRot);
	
	//포탄발사
	if(Input.GetButtonDown("Fire1")){
		var spPoint = GameObject.Find("spawnPoint");
		AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);
		var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
		Instantiate(explotion, spPoint.transform.position, Quaternion.identity);
	}
	
}