#pragma strict

private var power = 1200;

var bullet : Transform;
var target : Transform;
var spPoint : Transform;
var explosion : Transform;
var snd : AudioClip;

private var ftime : float = 0.0;

function Start () {

}

function Update () {
	transform.LookAt(target);
	
	ftime += Time.deltaTime;
	
	var hit : RaycastHit;
	//var fwd = Vector3.forward;
	var fwd = transform.TransformDirection(Vector3.forward);
	
	
	
	
	//탐색 실패
	Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.red);
	if(Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
	if(hit.collider.gameObject.tag != "TANK" || ftime<2) return;
	Debug.Log(hit.collider.gameObject.name);
	
	//포구 앞의 화염
	Instantiate(explosion, spPoint.transform.position, spPoint.transform.rotation);
	
	//포탄
	var obj = Instantiate(bullet, spPoint.transform.position, Quaternion.identity);
	obj.rigidbody.AddForce(fwd * power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position); //포탄 발사음.
	ftime = 0;
}